/*
Noah Celuch
This file creates and tests the HashTable with multiple functions
Dr. Boatright, Object Oriented and Advanced Programmin
September 8, 2017
*/

#include "stdafx.h"
#include <vector>
#include <list>
#include <iostream>
#include <string>
#include <iterator>

//Templated HashTable class
template<typename T, typename K, typename HASHF>
class HashTable {
public:

	//Default constuctor
	HashTable() {
		hashVector.clear();
		capacity = 1;
		loadFactor = 1;
	}

	//Constructor taking capacity and loadFactor from the user
	HashTable(int capacityArg, float loadFactorArg) {
		capacity = capacityArg;
		loadFactor = loadFactorArg;

		//initializing hashVector with empty list of pairs
		std::list<std::pair<K, T>> empty;
		empty.clear();

		for (int i = 0; i < capacity; i++) {
			hashVector.push_back(empty);
		};
	}

	//function to insert value
	void insert(T value, K key) {
		std::pair<K, T> tempPair;
		tempPair = std::make_pair(key, value);

		//check if key already exists in table
		bool exists = false;
		for (auto iter = hashVector.at(hashFunction(key) % capacity).begin();
			iter != hashVector.at(hashFunction(key) % capacity).end(); iter++) {
			if (std::get<0>(*iter) == key) {
				exists = true;
			}
		}

		//insert key if it does not exist
		//otherwise print if key already exists
		if (!exists) {
			hashVector.at(hashFunction(key) % capacity).emplace_back(tempPair);

			elements++;

			//resize table if loadFactor has been exceeded
			if ((static_cast<float>(elements) / capacity) >= loadFactor) {
				hashResize();
			}
		}
		else {
			std::cout << "Error: Key already exists in table" << std::endl;
		}
	}

	//function to search for key in table
	T search(K key) {
		int index = hashFunction(key)%capacity;
		
		//search through list at index corresponding to hashFunction
		for (auto iter = hashVector.at(index).begin(); 
			iter != hashVector.at(index).end(); iter++) {
			if (std::get<0>(*iter) == key) {
				return std::get<1>(*iter);
			}
		}

		std::cout << "Key is not valid" << std::endl;
		return NULL;
	}

	//function to resize hashVector
	void hashResize() {
		//set temporary hash table equal to current hash table
		int tempCapacity = capacity;
		std::vector<std::list<std::pair<K, T>>>* temp = new std::vector<std::list<std::pair<K,T>>>(hashVector);

		capacity *= 2;
		hashVector.resize(capacity);

		//iterate through all items in temporary hash table
		for (int i = 0; i < tempCapacity; i++) {
			hashVector.at(i).clear();
			for (auto listIter = temp->at(i).begin();
				listIter != temp->at(i).end();
				listIter++) {
					//re-insert all items into hashVector based on new capacity
					hashVector.at(hashFunction(std::get<0>(*listIter))%capacity).emplace_back(*listIter);
			}
		}

		delete temp;
	}

private:
	std::vector<std::list<std::pair<K,T>>> hashVector;
	int capacity;
	int elements;
	float loadFactor;
	HASHF hashFunction;
};

//hash functor for ints
struct hashFunctionInt {
	int operator()(int key) {

		//from Introduction to Algorithms By Thomas H. Cormen
		float index = (sqrt(5) - 1) / 2;
		float temp;
		index *= key;
		index = modf(index, &temp);
		index *= 16384;

		return floor(index);
	}
};

//hash functor for floats
struct hashFunctionFloat {
	int operator()(float key) {

		//from Introduction to Algorithms By Thomis H. Cormen
		float index = (sqrt(5) - 1) / 2;
		float temp;
		index *= key;
		index = modf(index, &temp);
		index *= 16384;

		return floor(index);
	} 
};

//driver for ints
void intDriver() {
	std::cout << "Int Driver: " << std::endl;
	HashTable<int, int, hashFunctionInt> intTable(10, 0.75f);

	intTable.insert(1234, 14);
	intTable.insert(6338, 23);
	intTable.insert(7643, 62);
	intTable.insert(3453, 33);
	intTable.insert(3453, 49);
	intTable.insert(8473, 52);
	intTable.insert(6963, 56);
	//will resize on this insert
	intTable.insert(7934, 73);
	intTable.insert(4682, 68);
	intTable.insert(6423, 39);
	intTable.insert(1287, 11);
	intTable.insert(2342, 153);
	//will not insert, value already in table
	intTable.insert(3442, 62);

	std::cout << intTable.search(62) << std::endl;
	std::cout << intTable.search(33) << std::endl;
	std::cout << intTable.search(49) << std::endl;
	std::cout << intTable.search(73) << std::endl;
	//will not find key, key does not exist
	std::cout << intTable.search(1124) << std::endl;
	std::cout << std::endl;
		
}

//driver for floats
void floatDriver() {
	std::cout << "Float Driver: " << std::endl;
	HashTable<int, float, hashFunctionFloat> floatTable(10, 1.0f);

	floatTable.insert(1654, 6.233);
	floatTable.insert(2353, 5.37629);
	floatTable.insert(3423, 10.145);
	floatTable.insert(4343, 1.2345);
	floatTable.insert(5423, 3.37628);
	floatTable.insert(6473, 2.043);
	floatTable.insert(2345, 5.37628);
	floatTable.insert(5221, 3.453);
	floatTable.insert(839, 9.521);
	//will resize on this insert
	floatTable.insert(6839, 8.124);
	floatTable.insert(5322, 9.1235);
	floatTable.insert(2123, 223.3421);
	//will not insert, key already exists
	floatTable.insert(3425, 10.145);

	std::cout << floatTable.search(5.37629) << std::endl;
	std::cout << floatTable.search(5.37628) << std::endl;
	std::cout << floatTable.search(9.521) << std::endl;
	std::cout << floatTable.search(223.3421) << std::endl;
	//will not find value, key does not exist
	std::cout << floatTable.search(354.2341) << std::endl;
	std::cout << std::endl;
}

//driver for strings
void stringDriver() {
	std::cout << "String Driver: " << std::endl;
	HashTable<int, std::string, std::hash<std::string>> stringTable(100, 0.5f);

	stringTable.insert(1332, "key1");
	stringTable.insert(5322, "key2");
	stringTable.insert(5983, "item3");
	stringTable.insert(4345, "key323");
	stringTable.insert(5786, "key4");
	stringTable.insert(1236, "key6");
	stringTable.insert(6432, "item7");
	stringTable.insert(3432, "item8");
	stringTable.insert(3453, "key9");
	stringTable.insert(2354, "key7");
	stringTable.insert(3453, "key10");
	//neither of these two will insert
	//keys already exist
	stringTable.insert(3031, "key2");
	stringTable.insert(8492, "item7");

	std::cout << stringTable.search("item3") << std::endl;
	std::cout << stringTable.search("key2") << std::endl;
	std::cout << stringTable.search("item8") << std::endl;
	std::cout << stringTable.search("key10") << std::endl;
	//values will not be found, key does not exist
	std::cout << stringTable.search("key12345") << std::endl;
}

void driver() {
	intDriver();
	floatDriver();
	stringDriver();
}

int main() {
	
	driver();

    return 0;
}

